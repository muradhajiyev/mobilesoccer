package volley.mobilesoccer.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import volley.mobilesoccer.MyApplication;
import volley.mobilesoccer.R;
import volley.mobilesoccer.adapters.FavoritesAdapter;
import volley.mobilesoccer.utils.Storage;

/**
 * Created by Administrator on 06.11.2016.
 */
public class FavoritesFragment extends Fragment {

    List<String> categorylist;
    List<Integer> categoryIdList;
    List<Integer> list;
    ListView listView;
    Storage storage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.favorites_list, container, false);
        storage = Storage.getInstance(getContext());
        listView = (ListView) view.findViewById(R.id.bolme_fav_list);

        categorylist = new ArrayList<>();
        categoryIdList = new ArrayList<>();
        list = new ArrayList<>();





        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage("Zəhmət olmasa gözləyin ...");
        pd.show();


        String categories_url = "http://websport.azurewebsites.net/Mobile/GetCategories";
        JsonObjectRequest categoriesReq = new JsonObjectRequest(Request.Method.POST, categories_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray list = response.getJSONArray("categoryList");
                    for (int i=0; i<list.length(); i++){
                        JSONObject item = list.getJSONObject(i);
                        categoryIdList.add(item.getInt("Id"));
                        categorylist.add(item.getString("Name"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                String url = "http://websport.azurewebsites.net/Mobile/GetFavoriteList";
                JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i=0; i<response.length(); i++){
                            try {
                                JSONObject item = response.getJSONObject(i);
                                list.add(item.getInt("Id"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        pd.dismiss();
                        FavoritesAdapter adapter = new FavoritesAdapter(categorylist,categoryIdList,list, getActivity().getApplicationContext());
                        listView.setAdapter(adapter);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        String userid = storage.getUserId();
                        params.put("AppId", userid);
                        return params;
                    }
                };

                // Adding request to request queue
                MyApplication.getInstance().addToRequestQueue(request, "test");

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();

            }
        });





        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(categoriesReq, "test");



        return view;
    }
}
