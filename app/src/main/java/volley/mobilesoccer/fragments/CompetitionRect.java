package volley.mobilesoccer.fragments;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.loopj.android.http.AsyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import volley.mobilesoccer.MainActivity;
import volley.mobilesoccer.MyApplication;
import volley.mobilesoccer.R;
import volley.mobilesoccer.TeamActivity;
import volley.mobilesoccer.adapters.CompetitionAdapter;
import volley.mobilesoccer.adapters.CompetitionRectAdapter;
import volley.mobilesoccer.models.Competition;


public class CompetitionRect extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    ImageView loading10;
    AnimationDrawable animationDrawable;
    GridView gridView1;
    ArrayList<Competition> list;
    CompetitionRectAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ((MainActivity) getActivity()).setActionBarTitle("Squads");
        View view = inflater.inflate(R.layout.fragment_competition_rect,container,false);

        loading10 = (ImageView)view.findViewById(R.id.loading10);
        gridView1= (GridView)view.findViewById(R.id.gridView1);
        animationDrawable = (AnimationDrawable)loading10.getBackground();

        String tag_json_arry = "json_array_req";
        list = new ArrayList<>();
        adapter = new CompetitionRectAdapter(getContext(),list);
        gridView1.setAdapter(adapter);

        String url = "http://api.football-data.org/v1/competitions/?season=2016";
        animationDrawable.start();
        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {

                            for (int j = 0; j < response.length(); j++) {

                                JSONObject item = response.getJSONObject(j);
                                Integer id = item.getInt("id");
                                String name = item.getString("caption");
                                String shortname = item.getString("league");
                                String numberOfTeams = "Number Of Teams: "+item.getString("numberOfTeams");
                                String numberOfGames = "Number Of Games: "+item.getString("numberOfGames");
                                String season = item.getString("year");
                                Competition competition = new Competition(id, name, shortname,
                                        numberOfTeams, numberOfGames, season);
                                list.add(competition);
                            }


                            adapter.notifyDataSetChanged();
                            gridView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Competition competition = list.get(position);
                            Intent intent = new Intent(getActivity(), TeamActivity.class);
                            intent.putExtra("id",competition.getId());
                            intent.putExtra("caption", competition.getName());
                            intent.putExtra("league", competition.getShortName());
                            startActivity(intent);
                                }
                            });
                        } catch (Exception e) {

                        }
                        animationDrawable.stop();
                        loading10.setVisibility(View.INVISIBLE);


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());
                animationDrawable.stop();
                loading10.setVisibility(View.INVISIBLE);
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("X-Auth-Token", "7175220d226744269e5fee998421e70d");
                return headers;
            }
        };

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req, tag_json_arry);
        return view;
    }

}
