package volley.mobilesoccer.fragments;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import volley.mobilesoccer.DividerItemDecoration;
import volley.mobilesoccer.LeagueDetailActivity;
import volley.mobilesoccer.MyApplication;
import volley.mobilesoccer.R;
import volley.mobilesoccer.adapters.CompetitionAdapter;
import volley.mobilesoccer.adapters.FixtureAdapter;
import volley.mobilesoccer.models.Competition;
import volley.mobilesoccer.models.Fixture;

/**
 * Created by Administrator on 06.11.2016.
 */
public class FixtureTableFragment extends Fragment {

    private final String TAG = FixtureTableFragment.class.getName();
    FixtureAdapter adapter;
    RecyclerView recyclerView;
    List<Fixture> list;
    ImageView loading;
    AnimationDrawable animationDrawable;
    Integer league_id;

    TextView errormessage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fixture_table_layout, container, false);

        LeagueDetailActivity activity = (LeagueDetailActivity) getActivity();
        league_id = activity.getLeague_id();

        errormessage = (TextView) view.findViewById(R.id.errormessage);

        recyclerView = (RecyclerView) view.findViewById(R.id.competitions_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        list = new ArrayList<>();
        adapter = new FixtureAdapter(list,getContext());
        recyclerView.setAdapter(adapter);



        loading = (ImageView) view.findViewById(R.id.loading);
        animationDrawable = (AnimationDrawable) loading.getBackground();

        String tag_json_arry = "json_array_req";

        String url = "http://api.football-data.org/v1/competitions/"+league_id+"/fixtures?timeFrame=n20";
        animationDrawable.start();

        JsonObjectRequest request = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    JSONArray fixtures = response.getJSONArray("fixtures");
                    if (fixtures.length()>0){
                        for (int i=0; i<fixtures.length();i++){
                            JSONObject item = fixtures.getJSONObject(i);
                            String date = item.getString("date");
                            String matchday = item.getString("matchday");
                            String homeTeamName = item.getString("homeTeamName");
                            String awayTeamName = item.getString("awayTeamName");

                            Fixture fixture = new Fixture(date,matchday,homeTeamName,awayTeamName);
                            list.add(fixture);
                            adapter.notifyDataSetChanged();
                        }

                    }else{
                        errormessage.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                animationDrawable.stop();
                loading.setVisibility(View.INVISIBLE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                animationDrawable.stop();
                loading.setVisibility(View.INVISIBLE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("X-Auth-Token", "7175220d226744269e5fee998421e70d");
                return headers;
            }
        };





        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(request, tag_json_arry);





        return view;
    }


    private String formatDate(String date){

        // *** same for the format String below
        Date d = new Date(Long.valueOf(date));
        DateFormat df = new SimpleDateFormat("dd.MM.yy");

        return df.format(d);
    }
}
