package volley.mobilesoccer.fragments;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import volley.mobilesoccer.LeagueDetailActivity;
import volley.mobilesoccer.MyApplication;
import volley.mobilesoccer.R;
import volley.mobilesoccer.adapters.CupTableAdapter;
import volley.mobilesoccer.utils.CustomVolleyRequestQueue;
import volley.mobilesoccer.models.CupTable;

/**
 * Created by Administrator on 06.11.2016.
 */
public class LeagueTableFragment extends Fragment {

    private final String TAG = LeagueTableFragment.class.getName();
    Integer league_id;
    TableLayout table;
    TextView competition_name;
    private ImageLoader mImageLoader;
    ImageView loading;
    AnimationDrawable animationDrawable;

    private CupTableAdapter adapter;
    private HashMap<String,List<CupTable>> hashmap;
    List<CupTable> list;
    List<String> groups_list;
    private RecyclerView recyclerView;


    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LeagueDetailActivity activity = (LeagueDetailActivity) getActivity();
        league_id = activity.getLeague_id();

        if (league_id == 424 || league_id==440){
            view = inflater.inflate(R.layout.cup_table, container, false);
            recyclerView = (RecyclerView) view.findViewById(R.id.recycler_cup_table);
            hashmap = new HashMap<>();
            list = new ArrayList<>();
            groups_list = new ArrayList<>();
            adapter = new CupTableAdapter(list,groups_list, getContext());
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);


            String tag_json_obj = "json_array_req";
            String url = "http://api.football-data.org/v1/competitions/"+league_id+"/leagueTable";

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                    url, null,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());

                            try {
                                JSONObject groups = response.getJSONObject("standings");
                                Iterator<?> keys = groups.keys();
                                while(keys.hasNext()){
                                    String group_name = keys.next().toString();
                                    groups_list.add(group_name);
                                    JSONArray json_list = groups.getJSONArray(group_name);

                                    for (int i=0; i< json_list.length(); i++){
                                        JSONObject item = json_list.getJSONObject(i);
                                        String team = item.getString("team");
                                        String points = item.getString("points");
                                        String goals = item.getString("goals");
                                        String goalAgainst = item.getString("goalsAgainst");
                                        CupTable cupTable = new CupTable(group_name, team, points, goals, goalAgainst, null );
                                        list.add(cupTable);
                                    }
                                    hashmap.put(group_name, list);
                                }
                                adapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }




                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());

                }
            }) {

                /**
                 * Passing some request headers
                 * */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    headers.put("X-Auth-Token", "7175220d226744269e5fee998421e70d");
                    return headers;
                }

            };

            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

        }else{
            view = inflater.inflate(R.layout.league_table, container, false);

            table = (TableLayout)view.findViewById(R.id.table_layout);
            competition_name = (TextView) view.findViewById(R.id.competition_name);
            loading = (ImageView) view.findViewById(R.id.loading);




            String tag_json_obj = "json_array_req";
            String url = "http://api.football-data.org/v1/competitions/"+league_id+"/leagueTable";

            animationDrawable = (AnimationDrawable) loading.getBackground();
            animationDrawable.start();
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                    url, null,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());

                            try {
                                String leagueCaption = response.getString("leagueCaption");
                                JSONArray standing = response.getJSONArray("standing");
                                competition_name.setText(leagueCaption);
                                for(int i=0;i<standing.length();i++){
                                    // get a reference for the TableLayout

                                    // create a new TableRow
                                    final TableRow tableRow = (TableRow) getActivity().getLayoutInflater().inflate(R.layout.tablerow, null);

                                    // create a new TextView for showing xml data
                                    TextView order = (TextView) tableRow.findViewById(R.id.order);
/*                                    ImageView imageView = (ImageView) tableRow.findViewById(R.id.club_logo);*/
                                    NetworkImageView imageView = (NetworkImageView) tableRow.findViewById(R.id.networkImageView);
                                    TextView league_name = (TextView) tableRow.findViewById(R.id.league_name);
                                    TextView points= (TextView) tableRow.findViewById(R.id.points);
                                    TextView goals = (TextView) tableRow.findViewById(R.id.goals);


                                    JSONObject item = standing.getJSONObject(i);

                                    order.setText(""+(i+1));
                                    league_name.setText(item.getString("teamName"));
                                    points.setText(item.getString("points"));
                                    goals.setText(item.getString("goals")+":"+item.getString("goalsAgainst"));

                                    mImageLoader = CustomVolleyRequestQueue.getInstance(getContext())
                                            .getImageLoader();
                                    //Image URL - This can point to any image file supported by Android
                                    String url = item.getString("crestURI");
                                    if (url.contains("http:")){
                                        url = url.replace("http","https");
                                    }
                                    mImageLoader.get(url, ImageLoader.getImageListener(imageView,
                                            R.drawable.ic_menu_camera, android.R.drawable
                                                    .ic_dialog_alert));
                                    imageView.setImageUrl(url, mImageLoader);






                                    // add the TableRow to the TableLayout
                                    table.addView(tableRow, new TableLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
                                }



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            animationDrawable.stop();
                            loading.setVisibility(View.INVISIBLE);

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    animationDrawable.stop();
                    loading.setVisibility(View.INVISIBLE);

                }
            }) {

                /**
                 * Passing some request headers
                 * */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    headers.put("X-Auth-Token", "7175220d226744269e5fee998421e70d");
                    return headers;
                }

            };

            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        }



        return view;
    }
}
