package volley.mobilesoccer.fragments;

import android.app.ProgressDialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import volley.mobilesoccer.DividerItemDecoration;
import volley.mobilesoccer.MainActivity;
import volley.mobilesoccer.MyApplication;
import volley.mobilesoccer.R;
import volley.mobilesoccer.adapters.CompetitionAdapter;
import volley.mobilesoccer.models.Competition;

/**
 * Created by Administrator on 05.11.2016.
 */
public class CompetitionsFragment extends Fragment {

    private final String TAG = CompetitionsFragment.class.getName();
    CompetitionAdapter adapter;
    RecyclerView recyclerView;
    List<Competition> list;
    ImageView loading;
    AnimationDrawable animationDrawable;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.competitions, container, false);

        // set the action bar title
        ((MainActivity) getActivity()).setActionBarTitle("Leagues");

        recyclerView = (RecyclerView) view.findViewById(R.id.competitions_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        list = new ArrayList<>();
        adapter = new CompetitionAdapter(list,getContext());
        recyclerView.setAdapter(adapter);

        loading = (ImageView) view.findViewById(R.id.loading);
        animationDrawable = (AnimationDrawable) loading.getBackground();

        String tag_json_arry = "json_array_req";

        String url = "http://api.football-data.org/v1/competitions/?season=2016";
        animationDrawable.start();
        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        for (int i=0; i< response.length(); i++){
                            try {
                                JSONObject item = response.getJSONObject(i);
                                if (item.getInt("id") != 432){
                                    Integer id = item.getInt("id");
                                    String name = item.getString("caption");
                                    String shortname = item.getString("league");
                                    String numberOfTeams = "Number Of Teams: "+item.getString("numberOfTeams");
                                    String numberOfGames = "Number Of Games: "+item.getString("numberOfGames");
                                    String season = item.getString("year");
                                    Competition competition =
                                            new Competition(id, name, shortname, numberOfTeams, numberOfGames, season);
                                    list.add(competition);

                                    adapter.notifyDataSetChanged();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        animationDrawable.stop();
                        loading.setVisibility(View.INVISIBLE);


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                animationDrawable.stop();
                loading.setVisibility(View.INVISIBLE);
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("X-Auth-Token", "7175220d226744269e5fee998421e70d");
                return headers;
            }
        };



        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(req, tag_json_arry);

        return view;
    }
}
