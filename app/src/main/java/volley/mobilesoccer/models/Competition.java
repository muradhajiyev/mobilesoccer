package volley.mobilesoccer.models;

/**
 * Created by Administrator on 05.11.2016.
 */
public class Competition {

    private Integer id;

    private String name,
            shortName, numberOfTeams,
            numberOfGames, season;

    public Competition(Integer id, String name, String shortName, String numberOfTeams, String numberOfGames, String season) {
        this.id = id;
        this.name = name;
        this.shortName = shortName;
        this.numberOfTeams = numberOfTeams;
        this.numberOfGames = numberOfGames;
        this.season = season;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getNumberOfTeams() {
        return numberOfTeams;
    }

    public void setNumberOfTeams(String numberOfTeams) {
        this.numberOfTeams = numberOfTeams;
    }

    public String getNumberOfGames() {
        return numberOfGames;
    }

    public void setNumberOfGames(String numberOfGames) {
        this.numberOfGames = numberOfGames;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }
}
