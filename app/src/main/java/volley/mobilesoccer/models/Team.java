package volley.mobilesoccer.models;

/**
 * Created by Eldar on 11/5/2016.
 */

public class Team {


    String linkPlayer;
    String name;
    String shortName;
    String squadMarketValue;
    String crestUrl;

    public Team(String linkPlayer , String name , String shortName ,String squadMarketValue , String crestUrl){

        this.linkPlayer =linkPlayer;
        this.name = name;
        this.shortName = shortName;
        this.squadMarketValue = squadMarketValue;
        this.crestUrl = crestUrl;
    }

    public String getLinkPlayer() {
        return linkPlayer;
    }

    public void setLinkPlayer(Integer id) {
        this.linkPlayer = linkPlayer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getSquadMarketValue() {
        return squadMarketValue;
    }

    public void setSquadMarketValue(String squadMarketValue) {
        this.squadMarketValue = squadMarketValue;
    }

    public String getCrestUrl() {
        return crestUrl;
    }

    public void setCrestUrl(String crestUrl) {
        this.crestUrl = crestUrl;
    }

}
