package volley.mobilesoccer.models;

/**
 * Created by Eldar on 11/5/2016.
 */

public class Player {


    String name;
    String position;
    String jerseyNumber;
    String dateOfBirth;
    String nationality;

    public Player(
            String name,
            String    position,
            String   jerseyNumber,
            String         dateOfBirth,
            String         nationality
    ){

        this.name = name;
        this.jerseyNumber = jerseyNumber;
        this.position = position;
        this.dateOfBirth = dateOfBirth;
        this.nationality = nationality;


    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getJerseyNumber() {
        return jerseyNumber;
    }

    public void setJerseyNumber(String jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }


}
