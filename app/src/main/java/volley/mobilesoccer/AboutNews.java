package volley.mobilesoccer;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.loopj.android.http.AsyncHttpClient;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import volley.mobilesoccer.models.News;
import volley.mobilesoccer.models.NewsAbout;

public class AboutNews extends AppCompatActivity {


    ViewPager viewPager;
    PagerAdapter adapter;
    Integer id;
    TextView xeberheader, txtDate, txtViews;
    WebView webView;
    String unikal_idget;
    String date, news_url;
    ImageView news_bckgrnd;

    ScrollView scrollViewabout;

    FragmentManager fm;

    String TAG = "AboutNews";


    AsyncHttpClient client;
    FragmentTransaction transaction;


    ArrayList<NewsAbout> arrayList;
    ArrayList<News> relatedarrayList;


    AnimationDrawable animationDrawable;
    GridView relatedListView;

    News news;


    NewsAbout newsAbout;
    NewsAbout newsAbout1;
    NewsAbout newsAboutForViewPager;
    AboutNews aboutNews;


    FrameLayout frameLayoutshare;
    ImageView loading8,shadow_news;
    ImageView imgUrl, related_reklam, back;

    TextView content;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newsabout);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("About");

        scrollViewabout = (ScrollView) findViewById(R.id.scrollabout);
        loading8 = (ImageView) findViewById(R.id.imgLoding8);
        imgUrl = (ImageView) findViewById(R.id.imgUrl);
        xeberheader = (TextView) findViewById(R.id.xeberheader);
        txtDate = (TextView) findViewById(R.id.txtDate);
        txtViews = (TextView) findViewById(R.id.txtViews);
        //back = (ImageView) findViewById(R.id.back);
        //webView = (WebView) findViewById(R.id.webview);
        content = (TextView) findViewById(R.id.content);


        shadow_news = (ImageView) findViewById(R.id.shadow_news);


        animationDrawable = (AnimationDrawable) loading8.getBackground();

        Intent intent = getIntent();

        if (intent != null) {
            id = intent.getIntExtra("Newid", 0);
            date = intent.getStringExtra("date");
            news_url = intent.getStringExtra("news_url");

        }
/*
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });*/

        String url = "http://websport.azurewebsites.net/Mobile/GetNews/" + id;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    arrayList = new ArrayList<NewsAbout>();
                    relatedarrayList = new ArrayList<News>();
                    JSONObject item = response;
                    String publishDate = item.getString("PublishDate");
                    if (publishDate != "null") {
                        publishDate = publishDate.replace("/Date(", "").replace(")/", "");
                    }
                    Date d = new Date(Long.valueOf(publishDate));
                    DateFormat df = new SimpleDateFormat("dd.MM.yy");
                    newsAbout = new NewsAbout(item.getString("Title"), item.getString("Content"), item.getString("PhotoUrl"), df.format(d).toString());

                    xeberheader.setText(newsAbout.getTitle());
                    txtDate.setText(df.format(d).toString());

                    content.setText(newsAbout.getContent());

                    Picasso picasso = Picasso.with(getApplicationContext());
                    picasso.load(newsAbout.getPhoto_url()).into(shadow_news,
                            new Callback() {
                                @Override
                                public void onSuccess() {
                                    loading8.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void onError() {
                                }
                            });


                } catch (Exception e) {
                    Log.e("Exception", e + "Saal,,,,");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (isNetworkAvailable()) {

                } else {
                    Toast.makeText(getApplicationContext(), R.string.new_toast, Toast.LENGTH_SHORT).show();
                    loading8.setVisibility(View.INVISIBLE);
                }
            }
        });

        MyApplication.getInstance().addToRequestQueue(request, "Get News Detail");
    }


    // check if network available
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() ==  android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}