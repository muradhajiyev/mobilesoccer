package volley.mobilesoccer.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import volley.mobilesoccer.LeagueDetailActivity;
import volley.mobilesoccer.R;
import volley.mobilesoccer.models.Competition;

/**
 * Created by Administrator on 05.11.2016.
 */
public class CompetitionAdapter extends RecyclerView.Adapter<CompetitionAdapter.MyViewHolder> {

    List<Competition> list;
    Context mContext;

    public CompetitionAdapter(List<Competition> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.competition_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Competition competition = list.get(position);
        holder.name.setText(competition.getName());
        holder.numberOfTeams.setText(competition.getNumberOfTeams());
        holder.numberOfGames.setText(competition.getNumberOfGames());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, numberOfTeams, numberOfGames;
        public ImageView iscup;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            numberOfTeams = (TextView) itemView.findViewById(R.id.numberofteams);
            numberOfGames = (TextView) itemView.findViewById(R.id.numberofgames);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            System.out.println("position: "+ getPosition());


                Intent intent = new Intent(mContext.getApplicationContext(), LeagueDetailActivity.class);
                intent.putExtra("league_id", list.get(getPosition()).getId());
                mContext.startActivity(intent);





        }
    }
}
