package volley.mobilesoccer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import volley.mobilesoccer.R;
import volley.mobilesoccer.models.Player;

/**
 * Created by Eldar on 11/5/2016.
 */

public class PlayerAdapter extends RecyclerView.Adapter<PlayerAdapter.MyViewHolder> {

    List<Player> list;
    Context mContext;

    public PlayerAdapter(List<Player> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public PlayerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.playerlistitem, parent, false);
        return new PlayerAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PlayerAdapter.MyViewHolder holder, int position) {
        Player player = list.get(position);


        holder.name.setText(player.getName());
        holder.jerseyNumber.setText(player.getJerseyNumber());
        holder.position.setText(player.getPosition());
        holder.dateOfBirth.setText(player.getDateOfBirth());
        holder.nationality.setText(player.getNationality());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, position, jerseyNumber, dateOfBirth, nationality;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            jerseyNumber = (TextView) itemView.findViewById(R.id.jerseyNumber);
            position = (TextView) itemView.findViewById(R.id.position);
            dateOfBirth = (TextView) itemView.findViewById(R.id.dateOfBirth);
            nationality = (TextView) itemView.findViewById(R.id.nationality);
        }

    }
}
